Task-CX.md

- Undertake [Task-C2](./Task-C2.md)
- Divide the target into 5 slices and accumulate the energy in each one separately
    - Revise the detector construction to add volumes inside the target 5 box volumes along the X direction.  
    - Give each one a different copy number, from 0 to 4. 
    - Accumulate energy deposition in each slice separately, inside a vector
```
    // In YourSteppingAction.hh add:
    G4double fEdepSlice[5];            // Simple alternative
    // std::vector<G4double> fEdepSlice;  // Modern alternative - DON'T use both !!
    
    // Now revise YourSteppingAction::UserSteppingAction to use it.
```
     
- Review Example B4c to see how to create tracker sensitive detector (store dE, x, y, z) or calorimeter sensitive detector