Day-3-topics.md
===============

Initial items
-------------
- Review homework & answer questions
- Follow-up concepts from homework - extended concepts
- Reset set-up of hands-on

New topics
----------
- Extracting information - scoring and hits
- User Actions - intro

Hands-on
--------
- [Task-C1](./Task-C1.md) - Use stepping action to collect energy deposit 
- [Task-C2](./Task-C2.md) - Use built-in scorer to collect energy deposit - in slices

Homework 
--------
- [Task-CX](./Task-CX.md) - Multiple tasks, some optional

Developers
----------
- User Actions: John / Mihaly
- Scorers:   John
- Sensitive: John
