# Geant4 First Steps Course

Exercises and related instructions for the course ["Getting Started with Geant4"](https://indico.cern.ch/event/1014059/), 25-31st May 2021

The course is held over five half days, with some lectures to set the scene, 
several hands-on exercises per day and some 'homework' exercises.

[Day 1](day1) - Introduction to Geant4, its Virtual Machine, and describing Geometry

[Day 2](day2) - Creating materials, generating primaries and visualising our setup

[Day 3](day3) - Extracting information from a simulation - hits, user action

[Day 4](day4) - Magnetic fields and physics

[Day 5](day5) - Interacting with Geant4 applications, and more

If you have corrections/comments please let us know during the course, preferably over the MatterMost channel!

Help for developers: 
 [Markdown cheat-sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
