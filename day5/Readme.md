# Day 5 hands-on topics
=======================

## Initial items
-------------
- Review homework & answer questions
- Follow-up concepts from homework - if any
- Reset set-up of hands-on

## New topics
- Hadronic Physics
- Magnetic field : creating one and enabling Geant4 to integrate track motion
- Multi-threading
- Run physical setup (slab) and compare with data (=> motivate Advanced)

## Hands-on

To start please download [Day5-v01.tar](https://indico.cern.ch/event/1014059/contributions/4307740/attachments/2248728/3825332/Day5-v01.tar)

- [Task E1](./Task-E1.md) A first taste of hadrons and physics lists with hadronic physics 
- [Task E2](./Task-E2.md) Magnetic field
- [Task E3](./Task-E3.md) Run multi-threading

Homework - all optional to exercise and extend concepts
--------

Developers
----------
- Hadronic physics: Alberto
- Magnetic field:  John
- Multi-threading: John
- Comparing full example with physics results: Mihaly