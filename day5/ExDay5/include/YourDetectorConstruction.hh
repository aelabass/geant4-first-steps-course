// This class is mandatory: descriptin of the detector
#ifndef YOURDETECTORCONTRUCTION_HH
#define YOURDETECTORCONTRUCTION_HH

#include "G4VUserDetectorConstruction.hh"


// forward declaration only !!!
class YourDetectorMessenger;

class G4Material;
class G4String;
class G4LogicalVolume;

class YourDetectorConstruction : public G4VUserDetectorConstruction
{
  // Method declaration:
  public:

    // CTR & DTR
    YourDetectorConstruction();
    virtual ~YourDetectorConstruction();

    // The base class has the (only one) pure virtual method Construct() which
    // is invoked by the G4RunManager when it's Initialize() method is invoked.
    // The Construct() method must return the G4VPhysicalVolume pointer which
    // represents the world volume.
    // Your detector description must be implemented here in this method.
    virtual G4VPhysicalVolume* Construct() override;

    //
    // Further (custom) methods implemnted by the user for this application:

    // Public methods to set/get the target material: G4 NIST materials
    void              SetTargetMaterial(const G4String& nistMatName);
    const G4Material* GetTargetMaterial() const  { return fTargetMaterial; }

    // Public method to obtain the proper gun-position depending on the detector
    G4double GetGunXPosition() { return fGunXPosition; }

    virtual void ConstructSDandField() override; 

  // Data member declaration
  private:

    // The detector messenger pointer: to set the target material
    YourDetectorMessenger* fDetMessenger;

    // Target material
    G4Material*            fTargetMaterial;

    // The target thickness i.e. its (full) x-size (YZ size will be set 10x this)
    G4double               fTargetThickness;

    // The midpoint between the target and the world on the negative x-side
    G4double               fGunXPosition;
};

#endif
