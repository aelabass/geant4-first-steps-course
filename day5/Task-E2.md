
## Magnetic Field

### Adding a field to our setup

Let's add a magnetic field to the example in ExDay5.

We will YourDetectorConstruction.cc and add the method

```
 YourDetectorConstruction::ConstructSDandField()
 {
    G4MagneticField* magField =   
         new G4UniformMagField(G4ThreeVector(2.*tesla,0.,0.) ); // Fixed )   
```
Then we will add this field to the world volume (directly or via the transportation manager.)

```
    // 1. Find the transportation manager first
    // Your CODE HERE ----

    // 2. Get the existing field manager (more than adequate)
    G4FieldManager* fieldMgr= transportationManager-> GetFieldManager();  // Fixed Manager*

    // 3. Register the field with this field manager 
    // Your CODE HERE ----

    // 4. Create the equation of motion, and default RK integration classes
    fieldMgr->CreateChordFinder(magField); // Default parameters
 }
```

Now compile and run. 
```
/run/initialize
/control/execute vis.mac
/run/beamOn 1
```
Check to see whether you can see particles deflecting.  Choose a 20 MeV 'mu+' initial particle using
```
/gun/particle mu+
/gun/energy  20 MeV
```
Experiment with different initial directions, along the x-, y- and z axis.
```
/gun/direction 0 0 1
/run/beamOn 5
```
Change the energy - is the track changing?

Change the particle type to 'mu-'. Do you see a different deflection of the muon track ?

Choose an imaginary particle which sees the magnetic field, but no physics interaction
```
/gun/particle chargedgeantino
```

### Ensure that there is no magnetic field in the target
Override the magnetic field in the 'target' physical volume with a new G4FieldManager which a null-pointer for the G4Field.

```
    G4LogicalVolume* targetLogical = fTargetPhysicalVolume-GetLogicalVolume();

```

### Ensuring 'good' accuracy for tracking in field

```

```

### Create our own quadrupole. magnetic field

Let's create a new class for a quadrupole magnetic field with a center at (0,0,0) and 
```
```

In a new file include/YourQuadrupoleMagField.hh let's 
```
//  What include files are needed ???
// #include ".hh"
// #include ".hh"

class YourQuadrupoleMagField : public G4MagneticField
{
   YourQuadrupoleMagField(G4double gradient);
   ~YourQuadrupoleMagField(){}

    void GetFieldValue(const G4double yTrack[],
                             G4double B[]     ) const;
  private:
    G4double          fGradient = 0.0;
}
```
In the source file YourQuadrupoleMagField.cc we need to define a constructor
```
#include "YourQuadrupoleMagField.hh"

   YourQuadrupoleMagField::
   YourQuadrupoleMagField(G4double gradient)
     : G4MagneticField() 
   { 
     // save the gradient
     // Your CODE HERE
   }; 
```

and the GetFieldValue method:
```
// -------------------------------------------------------------------
void G4QuadrupoleMagField::GetFieldValue( const G4double PositionAndTime[4],
                                                G4double B[3]  ) const
{
  B[0] = fGradient * PositionAndTime.y();
  B[1] = fGradient * PositionAndTime.x();  
  B[2] = 0.0;
}
```



