
## Multithreading

First let's run a hundred events in the sequential application.
```
/run/verbose 1
/run/initialize

/gun/particle e+
/gun/energy   30 GeV
/run/beamOn  100
```

Next let's enable multi-threading in our application.

Edit yourMainApplication.cc and change the RunManager
```
   G4RunManager* runManager = new G4RunManager();   // Sequential
```
Change this to
```
   G4RunManager* runManager =    G4RunManager* runManager = 
     G4RunManagerFactory::CreateRunManager(G4RunManagerType::Default); // Likely MT
```

Run your application again.  Do you see a difference in the speed - the number of events per second?

## Verbose output

Now let's make the run verbose
```
/gun/energy  20 MeV
/tracking/verbose 1
/run/beamOn  2
```

You will see that the output of the different events is intermingled
```

```


## Controling the number of threads

Now run again and choose the number of threads before the first G4 run:

```
/run/numberOfThreads 3
/run/initialize

/run/beamOn 3
```

Change the main program to do the same:
```
G4RunManager::SetNumberOfThreads(3);
```
before calling the RunManager's Initialize method (which typically is done by calling the UI manager's methods to process a batch file or ask the user for commands interactively.)

### Controling the output

Now let's select the output of one thread only:
```
/control/cout/ignoreThreadsExcept 1
```

Alternatively we can buffer all the output of threads:
```
/control/cout/useBuffer true
```
By default your output will kept until the end of the simulation, and then output.

Otherwise you can select to output them into different files, one per thread / task:
```
/control/cout/setCoutFile  yourOutput-Run001-cout.log
/control/cout/setCerrFile  yourOutput-Run001-cerr.log
```
Check what files are created, and what they contain.

### Using different types of multi-threading

Try the different types:
```
// [Option #1] enum class G4RunManagerType: Default, Serial, MT, Tasking, TBB 
runMgr = G4RunManagerFactory::CreateRunManager( G4RunManagerType::Default, 4); 

// [Option #2] string:  “default”, “serial”, “mt”, “task”, “tbb” 
auto* runMgr = G4RunManagerFactory::CreateRunManager( "default", 4); 
auto* runMgr = G4RunManagerFactory::CreateRunManager( "task", 4); 
auto* runMgr = G4RunManagerFactory::CreateRunManager( "tbb", 4); 
```





