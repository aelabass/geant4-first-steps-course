
Getting day 1 exercises
-----------------------
```shell
cd ~
mkdir g4
cd g4
```

There are two alternative to get the material for today. If you are familiar with git, you can use #2. Otherwise:

1) Download the tar archive file from Indico [Day1-v2.tar](https://indico.cern.ch/event/1014059/contributions/4307537/attachments/2250222/3817074/Day1-v2.tar) to your G4 virtual machine using firefox.

Don't open it - just save it (in Downloads)

Create a new ~/g4/exercises directory, and extract it there:

```shell
cd ~/g4
mkdir exercises
cd exercises

tar xvf ~/Downloads/Day1-v2.tar
ls
```

2) if you are familiar with 'git', fetch it directly from this [gitlab repository](https://gitlab.cern.ch/japost/geant4-first-steps-course/-/tree/master/)

```shell
cd ~/g4
git clone https://gitlab.cern.ch/japost/geant4-first-steps-course.git ./exercises
```

Check the Geant4 installation
-----------------------------
Let's check that Geant4 is configured

```shell
$ cd
$ env | grep G4INS
G4INSTALL=/usr/local/geant4.10.07.p01

$ ls $G4INSTALL
```

We locate the build tool 'cmake' - needed to configures builds of Geant4 application, and 'make' which does the compilation/build:
```shell
$ which cmake
/bin/cmake

$ whereis cmake
cmake: /usr/bin/cmake /usr/lib64/cmake /usr/share/cmake /usr/share/man/man1/cmake.1.gz

$ cmake --version 
cmake version 3.11.4

CMake suite maintained and supported by Kitware (kitware.com/cmake).

$ which make
$ make --version
```