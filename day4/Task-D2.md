
# Electromagnetic physics

## Exercise 1
Using UI command "/process/em/verbose 1" run your application and study log file:
   - a. which EM models are used for gamma, e-, e+.
   - b. what is cut in range?
   - c. what are production threshold for gamma, e-, e+

### Exercise 2
Using interactive mode study difference of Geant4 transport for electrons of different energy
   - a. 1 GeV, 10 MeV, 1 MeV, 0.1 MeV
   - b. see effect of cuts

### Exercise 3
First change target from silicon to lead , and increase target thickness to 5 cm in YourDetectorConstruction.cc (half length 2.5 cm) and the world thickness corresponding (half length 3.0 cm).  Recompile.

Then with user commands define primary 
- the vertex position in front of the target - e.g. (-2.51, 0, 0.)
- a corresponding direction pointing into the target - e.g. ( 1, 0, 0 )
- choose its particle type to e- 
- energy 500 MeV
Then:
- a. using interactive mode demonstrate EM shower for different cuts:
            0.1 mm, 1 mm, 1 cm, 10 cm, 100 cm
- b. change particle to mu+, change energy to 100 GeV

### Exercise 4
Change Physics List from FTFP_BERT_EMZ to FTFP_BERT_EMY using
   Physics List factory in the main of the application yourMainApplication.cc
```
    // (use reference physics list through the G4PhysListFactory)
    const G4String plName = "FTFP_BERT_EMZ";
    G4PhysListFactory plFactory;
    G4VModularPhysicsList *pl = plFactory.GetReferencePhysList( plName );
    runManager->SetUserInitialization( pl );
```
